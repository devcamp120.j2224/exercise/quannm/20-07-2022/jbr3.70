package com.devcamp.jbr370.jbr370;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StringLengthController {
    @CrossOrigin
    @GetMapping("/length")
    public int getStringLength() {
        String str = "Nguyen Minh Quan";
        int strLength = str.length();
        return strLength;
    }
}
